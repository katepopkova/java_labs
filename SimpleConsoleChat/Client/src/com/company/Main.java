package com.company;

import java.io.IOException;
import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        try {
            Socket client = new Socket("localhost", 9999);
            System.out.println("Connect to server...");
            Thread threadIn = new Thread(new SocketInputThread(client));// создание отдельного потока на считывание даных от сервера
            Thread threadOut = new Thread(new SocketOutputThread(client));// создание отдельного потока на ввод даных из клавиатуры
            threadIn.start();
            threadOut.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

