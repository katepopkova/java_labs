package com.company;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Ekaterina on 03.09.2016.
 */
public class SocketInputThread implements Runnable {
    private Socket mClient = null;

    public SocketInputThread(Socket aClient) {
        this.mClient = aClient;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Scanner mScanner = new Scanner(mClient.getInputStream());
                if (mScanner.hasNext()) {
                    String mInMessage = mScanner.nextLine();
                    System.out.println(mInMessage);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
