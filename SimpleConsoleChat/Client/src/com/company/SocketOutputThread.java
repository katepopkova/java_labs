package com.company;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Ekaterina on 03.09.2016.
 */
public class SocketOutputThread implements Runnable {
    private Socket mClient = null;

    public SocketOutputThread(Socket aClient) {
        this.mClient = aClient;
    }

    @Override
    public void run() {
        try {
            while (true) {
                PrintStream mPrinter = new PrintStream(mClient.getOutputStream());
                Scanner sc = new Scanner(System.in);
                String mOutMessage = sc.nextLine();
                mPrinter.println(mOutMessage);
                mPrinter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
