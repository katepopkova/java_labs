package com.company;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Ekaterina on 03.09.2016.
 */
public class SocketOutputThread implements Runnable {
    private Socket mSocket = null;

    public SocketOutputThread(Socket aSocket) {
        this.mSocket = aSocket;
    }

    @Override
    public void run() {
        try {
            //Scanner mScanner = new Scanner(mClient.getInputStream());
            while (true) {
                Scanner mScanner = new Scanner(mSocket.getInputStream());
                if (mScanner.hasNext()) {
                    String mInMessage = mScanner.nextLine();
                    System.out.println(mInMessage);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
