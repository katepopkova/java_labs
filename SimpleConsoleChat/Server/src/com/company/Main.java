package com.company;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(9999);
            System.out.println("Server started...");
            while (true) {
                Socket socket = server.accept();
                Thread threadIn = new Thread(new SocketInputThread(socket));// создание отдельного потока на считывание даных от сервера
                Thread threadOut = new Thread(new SocketOutputThread(socket));// создание отдельного потока на ввод даных из клавиатуры
                threadIn.start();
                threadOut.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
