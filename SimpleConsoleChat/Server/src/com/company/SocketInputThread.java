package com.company;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Ekaterina on 03.09.2016.
 */

public class SocketInputThread implements Runnable {
    private Socket mSocket = null;

    public SocketInputThread(Socket aSocket) {
        this.mSocket = aSocket;
    }

    @Override
    public void run() {
        try {

            while (true) {
                PrintStream mPrinter = new PrintStream(mSocket.getOutputStream());
                Scanner sc = new Scanner(System.in);
                String mOutMessage = sc.nextLine();
                mPrinter.println(mOutMessage);
                mPrinter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
